from django.urls import path
from . import views

urlpatterns = [
    path("upload_research", views.upload_research),
    path("get_research", views.get_research),
    path("get_research_status", views.get_research_status),

    path("view/<int:id>", views.view_research),
    path("kafka_processed", views.kafka_processed),
]
