from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from .slicer import handle_research
from .models import Research
from django.shortcuts import render
from django.views.decorators.csrf import csrf_exempt
from django.core import serializers
from .models import APIToken
import json, zipfile
import os


@csrf_exempt
def upload_research(request):
    response_data = {"ok": False}
    if request.method == "POST" and "research" in request.FILES and "token" in request.POST:
        token = request.POST["token"]
        if APIToken.objects.filter(token=token).count() > 0:
            research = request.FILES["research"]
            resp = handle_research(research, token)
            if not resp["ok"]:
                response_data["error"] = "Something goes wrong on server"
                return HttpResponse(json.dumps(response_data), content_type="application/json")
            del resp["ok"]
            response_data["result"] = resp
            response_data["ok"] = True
            return HttpResponse(json.dumps(response_data), content_type="application/json")
        response_data["error"] = "Invalid token"
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    response_data["error"] = "Invalid request"
    return HttpResponse(json.dumps(response_data), content_type="application/json")


@csrf_exempt
def get_research(request):
    response_data = {"ok": False}
    if request.method == "POST" and "research" in request.POST and "token" in request.POST:
        token = request.POST["token"]
        research_id = request.POST["research"]
        if APIToken.objects.filter(token=token).count() == 0:
            response_data["error"] = "Invalid token"
            return HttpResponse(json.dumps(response_data), content_type="application/json")

        research_q = Research.objects.filter(id=research_id)
        if research_q.count() != 1:
            response_data["error"] = "Research does not exists"
            return HttpResponse(json.dumps(response_data), content_type="application/json")

        research = research_q.first()

        if research.token != token:
            response_data["error"] = "Access error"
            return HttpResponse(json.dumps(response_data), content_type="application/json")

        research_json = serializers.serialize("json", research_q)
        response_data["ok"] = True
        response_data["research"] = research_json
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    response_data["error"] = "Invalid request"
    return HttpResponse(json.dumps(response_data), content_type="application/json")


@csrf_exempt
def get_research_status(request):
    response_data = {"ok": False}
    if request.method == "POST" and "research" in request.POST and "token" in request.POST:
        token = request.POST["token"]
        research_id = request.POST["research"]
        if APIToken.objects.filter(token=token).count() == 0:
            response_data["error"] = "Invalid token"
            return HttpResponse(json.dumps(response_data), content_type="application/json")

        research_q = Research.objects.filter(id=research_id)
        if research_q.count() != 1:
            response_data["error"] = "Research does not exists"
            return HttpResponse(json.dumps(response_data), content_type="application/json")

        research = research_q.first()

        if research.token != token:
            response_data["error"] = "Access error"
            return HttpResponse(json.dumps(response_data), content_type="application/json")

        response_data["ok"] = True
        response_data["status"] = research.processed
        return HttpResponse(json.dumps(response_data), content_type="application/json")
    response_data["error"] = "Invalid request"
    return HttpResponse(json.dumps(response_data), content_type="application/json")


def view_research(request, id):
    res = Research.objects.filter(id=id)
    if res.count() != 1:
        return HttpResponse("404")
    
    res = res[0]
    nods = []
    if res.predictions_nods != "":
        nods = json.loads(res.predictions_nods)
    return render(request, "Slicer/view_research.html", {
                "research": res,
                "nods": nods,
            }
        )


@csrf_exempt
def kafka_processed(request):
    if request.method == "POST" and "data" in request.POST:
        msg = json.loads(request.POST["data"])

        if msg["code"] == "success":
            path = msg["path"]
            research_id = int(msg["id"])

            research = Research.objects.filter(id=research_id)

            if research.count() != 1:
                print("Invalid research id recieved from kafka!")
                return HttpResponse("Invalid research id recieved from kafka!")


            def zipfolder(path, ziph):
                for root, dirs, files in os.walk(path):
                    for f in files:
                        print(os.path.join(root, f))
                        ziph.write(os.path.join(root, f))

            dir_path = os.path.join("static/research_storage/results/experiments", path, "_")
            zipf = zipfile.ZipFile(f"static/research_storage/results/zips/{path}.zip", "w", zipfile.ZIP_DEFLATED)
            zipfolder(dir_path, zipf)
            zipf.close()

            research = research[0]
            research.predictions_dir = path
            research.predictions_nods = json.dumps(msg["nods"])
            research.processed = 1
            research.save()
        else:
            print("An error occured during the prediction!!!")

        return HttpResponse("OK")
    return HttpResponse("Invalid request")
