from django.contrib import admin
from .models import Research, APIToken

admin.site.register(Research)
admin.site.register(APIToken)